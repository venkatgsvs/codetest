package com.codetest.search;

/**
 * Created by venkata.subramanyam
 */

import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codetest.search.callback.OnSearchCallback;
import com.codetest.search.fragment.DashboardFragment;
import com.codetest.search.model.SearchResults;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_sub_header)
    TextView mTvSubHeader;
    @BindView(R.id.dashboard_search_view)
    SearchView searchView;
    private OnSearchCallback onSearchCallback;
    private SearchResults searchResults;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        searchView.setOnQueryTextListener(this);
        initCollapsingToolbar();
        try {
            Glide.with(this).load(R.drawable.music).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
        navigateFragment(new DashboardFragment(), null);
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(mTvSubHeader.getText().toString());
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
                if (onSearchCallback != null)
                    onSearchCallback.showSearch(isShow);
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mTvSubHeader.setText(query + " " + getString(R.string.albums));
        onSearchCallback.queryString(query);
        searchView.clearFocus();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    /**
     * Method to navigate fragment
     *
     * @param childFragment
     * @param tag
     */
    public void navigateFragment(Fragment childFragment, String tag) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (tag == null)
            fragmentTransaction.replace(R.id.drawer_layout, childFragment).commit();
        else {
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);//enter, exit
            fragmentTransaction.replace(R.id.drawer_layout, childFragment, tag).addToBackStack(tag).commit();
        }
    }

    public OnSearchCallback getOnSearchCallback() {
        return onSearchCallback;
    }

    /**
     * call back for search
     *
     * @param onSearchCallback
     */
    public void setOnSearchCallback(OnSearchCallback onSearchCallback) {
        this.onSearchCallback = onSearchCallback;
    }

    /**
     * get search results
     *
     * @return
     */
    public SearchResults getSearchResults() {
        return searchResults;
    }

    /**
     * set search results
     *
     * @param searchResults
     */
    public void setSearchResults(SearchResults searchResults) {
        this.searchResults = searchResults;
    }
}

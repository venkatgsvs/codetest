package com.codetest.search.callback;

/**
 * Created by venkata.subramanyam
 */
public interface OnSearchCallback {
    void queryString(String query);

    void showSearch(boolean show);
}

package com.codetest.search.callback;

/**
 * Created by venkata.subramanyam
 */
public interface OnResponseCallback {
    void genericResponseStatus(Object result);
}

package com.codetest.search.callback;

import android.view.View;

/**
 * Created by venkata.subramanyam
 */

public interface RecyclerViewClickListener {

    void onClick(View view, int position);
}

package com.codetest.search.network;

/**
 * Created by venkata.subramanyam
 */

import com.codetest.search.model.SearchResults;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ItunesApi {

    @GET("/search")
    Call<SearchResults> getSearchResults(@Query("term") String term);

}
package com.codetest.search.network;

import android.support.annotation.NonNull;

import com.codetest.search.callback.OnResponseCallback;
import com.codetest.search.model.SearchResults;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by venkata.subramanyam
 */

public class ApiUtility {

    private static ApiUtility mNetworkUtility = null;
    private static final String BASE_URL = "https://itunes.apple.com";

    /**
     * Thread safe singleton but not a double checking
     *
     * @return
     */
    public synchronized static ApiUtility getInstance() {
        if (mNetworkUtility == null)
            mNetworkUtility = new ApiUtility();
        return mNetworkUtility;
    }

    /**
     * retrofit object creation and returns object
     *
     * @return
     */
    private Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * get retrofit service layer of connection establishment and returns class obj
     *
     * @return
     */
    private ItunesApi getService() {
        Retrofit retrofit = getRetrofit();
        return retrofit.create(ItunesApi.class);
    }

    /**
     * Call Search API
     *
     * @param searchParams
     * @param mResponseCallback
     */
    public void doSearch(String searchParams, final OnResponseCallback mResponseCallback) {
        Call<SearchResults> call = getService().getSearchResults(searchParams);
        call.enqueue(new Callback<SearchResults>() {
            @Override
            public void onResponse(@NonNull Call<SearchResults> call, @NonNull Response<SearchResults> response) {
                SearchResults responseBody = response.body();

                if (response.isSuccessful()) {
                    mResponseCallback.genericResponseStatus(responseBody);
                } else {
                    mResponseCallback.genericResponseStatus(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchResults> call, @NonNull Throwable t) {
                mResponseCallback.genericResponseStatus(null);
                System.out.println("Fetching data from server : FAILED");
            }
        });

    }


}

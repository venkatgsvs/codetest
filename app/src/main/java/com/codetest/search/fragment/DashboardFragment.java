package com.codetest.search.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codetest.search.MainActivity;
import com.codetest.search.R;
import com.codetest.search.adapter.DashboardAdapter;
import com.codetest.search.callback.OnResponseCallback;
import com.codetest.search.callback.OnSearchCallback;
import com.codetest.search.callback.RecyclerViewClickListener;
import com.codetest.search.customview.GridSpacingItemDecoration;
import com.codetest.search.model.SearchResults;
import com.codetest.search.network.ApiUtility;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by venkata.subramanyam
 */

public class DashboardFragment extends Fragment implements SearchView.OnQueryTextListener, RecyclerViewClickListener {

    @BindView(R.id.recycler_view)
    RecyclerView mAlbumListView;
    @BindView(R.id.tv_results_view)
    TextView mTvResults;
    SearchView searchView;
    MenuItem myActionMenuItem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dashboard_fragment, container, false);
        ButterKnife.bind(this, rootView);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mAlbumListView.setLayoutManager(mLayoutManager);
        mAlbumListView.addItemDecoration(new GridSpacingItemDecoration(2, ((MainActivity) getActivity()).dpToPx(10), true));
        mAlbumListView.setItemAnimator(new DefaultItemAnimator());
        setRetainInstance(true);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        ((MainActivity) getActivity()).setOnSearchCallback(new OnSearchCallback() {
            @Override
            public void queryString(String query) {
                doSearch(query);
            }

            @Override
            public void showSearch(boolean show) {
                if (myActionMenuItem != null)
                    myActionMenuItem.setVisible(show);
            }
        });
        if (((MainActivity) getActivity()).getSearchResults() == null) {
            mTvResults.setText(R.string.welcome);
            mTvResults.setVisibility(View.VISIBLE);
            mAlbumListView.setVisibility(View.GONE);
        } else {
            mTvResults.setVisibility(View.GONE);
            mAlbumListView.setVisibility(View.VISIBLE);
            showAdapterData((((MainActivity) getActivity()).getSearchResults()));
        }

    }

    private void doSearch(String query) {
        ApiUtility.getInstance().doSearch(query, new OnResponseCallback() {
            @Override
            public void genericResponseStatus(Object result) {
                if (result != null) {
                    mTvResults.setVisibility(View.GONE);
                    mAlbumListView.setVisibility(View.VISIBLE);
                    SearchResults mSearchResults = (SearchResults) result;
                    ((MainActivity) getActivity()).setSearchResults(mSearchResults);
                    showAdapterData(mSearchResults);
                } else {
                    mTvResults.setVisibility(View.VISIBLE);
                    mAlbumListView.setVisibility(View.GONE);
                    mTvResults.setText(R.string.no_results_found);
                }
            }
        });
    }

    private void showAdapterData(SearchResults mSearchResults) {
        DashboardAdapter adapter = new DashboardAdapter(getActivity(), mSearchResults, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", ((DashboardAdapter) (mAlbumListView.getAdapter())).getItem(position));
                //the track name, the album name, artist name, the artwork in a circular picture, price and the release date.
                DetailsFragment detailsFragment = new DetailsFragment();
                detailsFragment.setArguments(bundle);
                ((MainActivity) getActivity()).navigateFragment(detailsFragment, "DetailsFragment");
            }
        });
        mAlbumListView.setAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.dashboard, menu);

        myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        myActionMenuItem.setVisible(false);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        doSearch(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public void onClick(View view, int position) {

    }
}

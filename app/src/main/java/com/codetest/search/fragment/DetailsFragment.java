package com.codetest.search.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.codetest.search.R;
import com.codetest.search.model.SearchResults;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by venkata.subramanyam
 */

public class DetailsFragment extends Fragment {

    @BindView(R.id.tv_dashboard_header)
    TextView mHeader;
    @BindView(R.id.tv_dashboard_footer)
    TextView mFooter;
    @BindView(R.id.im_dashboard)
    ImageView imArt;
    private static final String NEW_LINE = "\n";
    private static final String SPACE = " ";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.details_screen, container, false);
        ButterKnife.bind(this, rootView);
        setRetainInstance(true);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            SearchResults.Result sResult = (SearchResults.Result) getArguments().getSerializable("data");
            mHeader.setText(sResult.getTrackName());
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(sResult.getCollectionName()).append(NEW_LINE)
                    .append(sResult.getArtistName()).append(NEW_LINE)
                    .append(getString(R.string.price)).append(sResult.getCollectionPrice()).append(SPACE).append(sResult.getCurrency()).append(NEW_LINE)
                    .append(getString(R.string.release_date)).append(sResult.getReleaseDate().replace("T", SPACE).replace("Z", SPACE));
            mFooter.setText(stringBuilder.toString());
            try {
                Glide.with(this).load(sResult.getArtworkUrl60()).into(imArt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

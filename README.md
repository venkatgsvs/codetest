# CodeTest

Honestly didn't get a time to write Instumentation test cases in an hour. 
If i get more time, then i will create functional Unit test cases using instrumentation and for mocking an objects i will use Mockito Library and for UI test cases i will use Espresso

Design Approach: 
	1) Got an idea from one of my personal app called Manna Foods, i got an UI dashboard idea from that and enhanced a bit accordingly and
	2) Used collapse bar with search enabling and disabling feature on toolbar means there will be two search 


Architechture Approach: Used MVC (If i get a more time, i will use MVP with more robustness)

Design patterns Used: Singleton, Abstract (If i get more time, i will use Factory based pattern and Facade pattern in MVP where it increases more in performace aspects)

Thrid party libraries used:
	1) Retrofit for server calls
	2) Gson for parsin
	3) Glide for fetching and display images in imageview
	4) Circle Imageview to shape imageview in circle shape
	5) Butterknife for injection
